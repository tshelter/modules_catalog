import os

from datetime import datetime
import logging

REPOS = {
    "geektg": ("https://github.com/GeekTG/FTG-Modules/",
               "https://github.com/GeekTG/FTG-Modules/blob/main/",
               "https://raw.githubusercontent.com/GeekTG/FTG-Modules/main/"),

    "offrepo": ("https://gitlab.com/friendly-telegram/modules-repo/",
                "https://gitlab.com/friendly-telegram/modules-repo/-/blob/master/",
                "https://gitlab.com/friendly-telegram/modules-repo/-/raw/master/"),

    "d4n13l3k00": ("https://git.d4n13l3k00.ru/D4n13l3k00/FTG-Modules/",
                   "https://git.d4n13l3k00.ru/D4n13l3k00/FTG-Modules/src/branch/master/",
                   "https://git.d4n13l3k00.ru/D4n13l3k00/FTG-Modules/raw/branch/master/"),

    "fl1yd": ("https://github.com/Fl1yd/FTG-Modules/",
              "https://github.com/Fl1yd/FTG-Modules/blob/master/",
              "https://raw.githubusercontent.com/Fl1yd/FTG-Modules/master/"),

    "keyzend": ("https://github.com/KeyZenD/modules/",
                "https://github.com/KeyZenD/modules/blob/master/",
                "https://raw.githubusercontent.com/KeyZenD/modules/master/"),

    "tio_temma": ("https://github.com/diademma/ftg-/",
                  "https://github.com/diademma/ftg-/blob/main/",
                  "https://raw.githubusercontent.com/diademma/ftg-/main/"),

    "smoke": ("https://github.com/sm1ke000FriendlyTelegram/Friendly-Telegram/",
              "https://github.com/sm1ke000FriendlyTelegram/Friendly-Telegram/blob/main/",
              "https://raw.githubusercontent.com/sm1ke000FriendlyTelegram/Friendly-Telegram/main/"),

    "atikd": ("https://github.com/AtikD/ftgmodules/",
              "https://github.com/AtikD/ftgmodules/blob/main/",
              "https://raw.githubusercontent.com/AtikD/ftgmodules/main/"),

    "hitalo": ("https://github.com/HitaloSama/FTG-modules-repo/",
               "https://github.com/HitaloSama/FTG-modules-repo/blob/master/",
               "https://raw.githubusercontent.com/HitaloSama/FTG-modules-repo/master/")
}

ADMINS = {
    549729560,
}

BOT_TOKEN = "ID:TOKEN"
LOGGING_LEVEL = logging.INFO

MODULES_DIR = "modules"

CATALOG_DB_DIR = "databases/catalog"
BOT_DB_DIR = "databases"

os.makedirs(MODULES_DIR, exist_ok=True)
os.makedirs(CATALOG_DB_DIR, exist_ok=True)
os.makedirs(BOT_DB_DIR, exist_ok=True)

CATALOG_DB_FILE = f"modules{datetime.today().strftime('%Yx%mx%d')}.sqlite"

CATALOG_DB_URL = f"sqlite+ext+async:///{CATALOG_DB_DIR}/{CATALOG_DB_FILE}"
BOT_DB_URL = f"sqlite+async:///{BOT_DB_DIR}/bot.sqlite"
