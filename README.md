# Installing
Install magic mime-type library </br>
`apt install libmagic1` </br>
Install requirements </br>
`pip3 install -r requirements.txt`
#### About building peewee
Install Cython </br>
`pip3 install cython` </br>
Clone repo and cd </br>
`git clone https://github.com/coleifer/peewee.git` </br>
`cd peewee` </br>
Install dependencies </br>
`sudo apt install libsqlite3-dev libsqlite3-0` </br>
Build SQLiteExtension </br>
`python3 setup.py build_ext -i` </br>
Install library </br>
`python3 setup.py install -f` </br>
Change user_mask if installation fails without root </br>
`umask 0022` </br>
Done!

# Running bot
`python3 main.py`

If you want update modules db: </br>
`python3 update.py`
