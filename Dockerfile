FROM python:3.10-slim-bookworm

WORKDIR /app

RUN apt update && apt install libmagic1 libmagic-dev git gcc libsqlite3-dev libsqlite3-0 -y

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt cython

RUN git clone https://github.com/coleifer/peewee.git /opt/peewee \
    && cd /opt/peewee \
    && git checkout 3.17.6 \
    && python3 setup.py build_ext -i \
    && python setup.py install -f

COPY . .

CMD ["python", "main.py"]
