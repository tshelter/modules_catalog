not_registered = "Start PM with bot to use <a href='{link}'>*click*</a>"
error_occurred = "Error occurred!\n{error}"

not_found = "Not found!"

sample = "Module: {name}{mod_description}\n\nCommand: {command}{command_description}\n\nMatch accuracy: {dist}%"
urls_sample = "Link: {url}\nRawLink: <code>{raw_url}</code>"
description = "\nDescription: {description}"
