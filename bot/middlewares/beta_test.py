import logging

import config
from bot import db

from aiogram import types
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.dispatcher.handler import CancelHandler

logger = logging.getLogger(__name__)


class RegisteringMiddleware(BaseMiddleware):
    """Автоматическая регистрация"""

    def __init__(self):
        super(RegisteringMiddleware, self).__init__()

    @staticmethod
    async def on_pre_process_message(message: types.Message, _):
        """Only if in beta_test"""
        user = message.conf["user"]
        if config.PASSWORD in message.text:
            with db.db:
                db.AllowedUser.create(
                    user=user.id
                )

        with db.db:
            if not db.AllowedUser.join(db.User).select(1).where(db.AllowedUser.user.id == user.id):
                raise CancelHandler()

        return True

    @staticmethod
    async def on_process_callback_query(callback_query: types.CallbackQuery, _):
        """Only if in beta_test"""
        user = callback_query.conf["user"]

        with db.db:
            if not db.AllowedUser.join(db.User).select(1).where(db.AllowedUser.user.id == user.id):
                raise CancelHandler()

        return True
