def middlewares_init(dp):
    from . import registration
    dp.middleware.setup(registration.RegisteringMiddleware())