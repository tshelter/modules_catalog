import logging

from bot import managers as m
import strings as s

from aiogram import types
from aiogram.dispatcher.middlewares import BaseMiddleware
from aiogram.dispatcher.handler import CancelHandler

logger = logging.getLogger(__name__)


class RegisteringMiddleware(BaseMiddleware):
    """Автоматическая регистрация"""

    def __init__(self):
        super(RegisteringMiddleware, self).__init__()

    @staticmethod
    async def on_pre_process_message(message: types.Message, _):
        """Register users"""
        user = m.users.register(message.from_user.id)

        if message.chat.type == types.ChatType.PRIVATE and user.banned:
            logger.debug(f"User in ban: {message.from_user.id}")
            raise CancelHandler()  # те кто в бане даже ответа не достойны

        message.conf["user"] = user

        return True

    @staticmethod
    async def on_process_callback_query(callback_query: types.CallbackQuery, _):
        """If not registered, displays error"""
        registered, user = m.users.check_register(user_id=callback_query.from_user.id)
        if not registered:
            await callback_query.answer(s.not_registered)
            raise CancelHandler()

        callback_query.conf["user"] = user

        return True
