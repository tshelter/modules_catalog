import logging
from typing import Tuple

from playhouse.shortcuts import model_to_dict as mtd
from bot import db
import config

logger = logging.getLogger(__name__)


class UsersManager:
    @staticmethod
    def register(user_id: int) -> db.User:
        """
        Register user in database by one's id
        :param user_id: user's id
        :return: db.User object
        """
        with db.db:
            user, is_created = db.User.get_or_create(id=user_id)

        return user

    @staticmethod
    def check_register(user_id: int) -> Tuple[bool, db.User]:
        """
        Checks if user registered
        :param user_id:
        :return: bool, db.User
        """
        with db.db:
            user = db.User.get_or_none(id=user_id)
            return bool(user), user

    @staticmethod
    def ban(user_id: int) -> db.User:
        with db.db:
            user = db.User.get_or_create(id=user_id)
            user.banned = True
            user.save()

        logger.debug(f"Banned user {mtd(user)}")

        return user

    @staticmethod
    def unban(user_id: int) -> db.User:
        with db.db:
            user = db.User.get_or_create(id=user_id)
            user.banned = False
            user.save()

        logger.debug(f"Banned user {mtd(user)}")

        return user

    @staticmethod
    async def check_admin(user_id: int) -> bool:
        """
        Проверяет админ ли пользователь
        :param user_id:
        :return:
        """

        is_admin = user_id in config.ADMINS
        logger.debug(f"User {user_id} is {'' if is_admin else 'not'} admin")

        return is_admin


users = UsersManager()
