from peewee import Model, IntegerField, BooleanField, ForeignKeyField
from playhouse.db_url import connect
import config
import async_sqlite

async_sqlite.patch()

db = connect(config.BOT_DB_URL, autocommit=False)


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    id = IntegerField(null=False, unique=True, primary_key=True)  # уникальный айди пользователя
    banned = BooleanField(null=False, default=False)  # заблокирован ли пользователь

    class Meta:
        db_table = "users"


class AllowedUser(BaseModel):
    id = IntegerField(null=False, unique=True, primary_key=True)  # уникальный айди пользователя
    user = ForeignKeyField(User)

    class Meta:
        db_table = "allowed_users"


with db:
    db.create_tables([
        User
    ])
