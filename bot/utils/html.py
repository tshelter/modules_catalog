def escape_html(text):
    # from https://github.com/GeekTG/Friendly-Telegram/blob/b0f96773c5881470e3c3ca2531b3b79a94082001/friendly
    # -telegram/utils.py#L85
    """Pass all untrusted/potentially corrupt input here"""
    return str(text).replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")
