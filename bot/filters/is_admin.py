from aiogram import types
from aiogram.dispatcher.filters import BoundFilter
import config

from typing import Union


class IsAdmin(BoundFilter):
    key = "is_admin"

    def __init__(self, is_admin):
        self.is_admin = is_admin

    async def check(self, event: Union[types.Message, types.CallbackQuery]):
        return event.from_user.id in config.ADMINS

