def filters_init(dp):
    from . import is_admin
    dp.filters_factory.bind(is_admin.IsAdmin, event_handlers=[dp.message_handlers, dp.callback_query_handlers])
