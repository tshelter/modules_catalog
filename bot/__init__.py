import logging

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage
from aiogram.types import ParseMode
from aiogram import executor

from bot.filters import filters_init
from bot.handlers import handlers_init
from bot.middlewares import middlewares_init
from bot.error_handlers import error_handlers_init
import config

#  Уровень логирования
logging.basicConfig(level=config.LOGGING_LEVEL)
logger = logging.getLogger(__name__)

# Общая инициализация
bot = Bot(token=config.BOT_TOKEN, parse_mode=ParseMode.HTML)
dp = Dispatcher(bot, storage=MemoryStorage())


def setup_bot():
    filters_init(dp)
    middlewares_init(dp)
    handlers_init()
    error_handlers_init()


def main():
    setup_bot()

    executor.start_polling(dp, skip_updates=True)
    logger.info("Bot fully loaded")
