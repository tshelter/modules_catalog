from bot import dp, db
from catalog import modules_db
from aiogram import types


@dp.message_handler(commands=["stat", "stats"])
async def bot_ping(m: types.Message):
    users = db.User.select().where(db.User.banned >> False).count()
    mods = modules_db.Module.select().count()

    txt = "Users in bot: {users}\nModules in catalog: {mods}".format(
        users=users, mods=mods
    )

    await m.reply(txt)
