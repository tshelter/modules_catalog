from aiogram import types

from bot import dp
from catalog.utils import find_command
import strings as s


def get_search_buttons(s_type: str = "command", have_watcher: bool = None, page=0) -> types.InlineKeyboardMarkup:
    # TODO: use it
    keyboard = types.InlineKeyboardMarkup()

    print(keyboard)

    return keyboard


@dp.message_handler()
async def bot_search_enter(m: types.Message):
    command = m.text[1:] if m.text.startswith(".") else m.text

    flag = False
    for result, _ in await find_command(command):
        flag = True
        await m.answer(result, disable_web_page_preview=True)

    if not flag:
        await m.answer(s.not_found)
