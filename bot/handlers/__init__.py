def handlers_init():
    from . import simple_commands
    from . import cancel
    from . import find_command
    from . import inline

