import hashlib
from typing import List

from aiogram import types
from aiogram.utils.deep_linking import get_start_link

from bot import dp, managers
from catalog.utils import find_command
import strings as s


async def register_in_bot_fuck_you() -> List[types.InlineQueryResultArticle]:
    text = s.not_registered.format(link=await get_start_link('_inline'))
    input_content = types.InputTextMessageContent(text)
    result_id: str = hashlib.md5(text.encode()).hexdigest()

    item = types.InlineQueryResultArticle(
        id=result_id,
        title=f'README',
        description="You have not PM with bot",
        input_message_content=input_content,
    )

    return [item]


@dp.inline_handler()
async def inline_command_search(i: types.InlineQuery):
    if not managers.users.check_register(i.from_user.id):
        return await i.answer(results=await register_in_bot_fuck_you(), cache_time=30)

    command = (i.query[1:] if i.query.startswith(".") else i.query) or "ping"

    items = [
        types.InlineQueryResultArticle(
            id=hashlib.md5(result.encode()).hexdigest(),
            title=f'Module: {raw_dict["name"]}',
            description=f'Command: {raw_dict["command"]}'
                        f'\nDescription: {raw_dict["command_description"] or "missing..."}',
            input_message_content=types.InputTextMessageContent(result, disable_web_page_preview=True),
        )
        for result, raw_dict in await find_command(command, limit=10)
    ]

    if not items:
        txt = s.not_found + "\n" + command
        return await i.answer(
            results=[types.InlineQueryResultArticle(
                id=hashlib.md5(txt.encode()).hexdigest(),
                title=txt,
                description=txt,
                input_message_content=types.InputTextMessageContent(txt),
            )],
            cache_time=43200  # 12h
        )

    await i.answer(results=items, cache_time=43200)
