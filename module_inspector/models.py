from __future__ import annotations

import ast
from dataclasses import dataclass
from typing import List, Dict, Union


@dataclass
class Module:
    name: str
    description: str
    commands_tree: Dict[str, str]
    have_watcher: bool
    url: str
    raw_url: str

    @staticmethod
    def parse(lines: str, url, raw_url) -> Union[Module, None]:
        try:
            file = ast.parse(lines)
        except SyntaxError:
            return None

        # get FTG module class
        try:
            module: ast.ClassDef = next(_ for _ in file.body if isinstance(_, ast.ClassDef) and _.name.endswith("Mod"))
        except StopIteration:
            return None

        name = module.name[:-3]

        commands_func: List[ast.AsyncFunctionDef]

        functions = [_ for _ in module.body
                     if isinstance(_, ast.AsyncFunctionDef)]
        commands_func = [_ for _ in functions if _.name.endswith("cmd")]
        have_watcher = bool([1 for _ in functions if _.name == "watcher"])
        commands_tree = {
            _.name.split("cmd")[0]: ast.get_docstring(_) for _ in commands_func
        }

        return Module(
            name=name,
            description=ast.get_docstring(module),
            commands_tree=commands_tree,
            have_watcher=have_watcher,
            url=url,
            raw_url=raw_url
        )

    # def __dict__(self) -> dict:
    #     return dict(
    #         name=self.name,
    #         description=self.description,
    #         commands_tree=self.commands_tree,
    #         have_watcher=self.have_watcher
    #     )

    def __str__(self):
        return str(dict(self.__dict__))
