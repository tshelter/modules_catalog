import ast
from typing import List


def get_lines(filename: str) -> str:
    with open(filename, "r") as f:
        return f.read()

