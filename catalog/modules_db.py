import peewee
from peewee import CharField, BooleanField, ForeignKeyField
from playhouse.sqlite_udf import levenshtein_dist

import async_sqlite
import config

from playhouse.db_url import connect

async_sqlite.patch()

db = connect(config.CATALOG_DB_URL, c_extensions=True)


class Base(peewee.Model):
    class Meta:
        database = db


class Module(Base):
    name = CharField(max_length=4096)
    description = CharField(max_length=4096, null=True)
    have_watcher = BooleanField()
    url = CharField(max_length=4096)
    raw_url = CharField(max_length=4096)

    class Meta:
        db_table = "modules"


class CommandsTree(Base):
    module = ForeignKeyField(Module)
    command = CharField(max_length=4096)
    description = CharField(max_length=4096, null=True)

    class Meta:
        db_table = "commands_trees"


with db:
    db.create_tables(
        [Module, CommandsTree]
    )
    db.register_function(levenshtein_dist, "levenshtein_dist")
