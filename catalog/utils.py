from typing import List, Tuple

from peewee import fn

from catalog import modules_db
from bot import utils
import strings as s


async def _generate(query: List[modules_db.CommandsTree]) -> List[Tuple[str, dict]]:
    return [
        (
            "\n".join((utils.escape_html(main_text), urls)),
            raw_dict
        )
        for main_text, urls, raw_dict in (
            (
                s.sample.format(
                    name=tree.module.name,
                    mod_description=s.description.format(
                        description=tree.module.description
                    )
                    if tree.module.description
                    else "",
                    command=tree.command,
                    command_description=s.description.format(
                        description=tree.description
                    )
                    if tree.description
                    else "",
                    dist=round(
                        (len(tree.command) - int(tree.dist)) / len(tree.command) * 100
                    ),
                ),
                s.urls_sample.format(
                    url=tree.module.url, raw_url=tree.module.raw_url
                ),
                {
                    "name": tree.module.name,
                    "mod_description": tree.module.description,
                    "command": tree.command,
                    "command_description": tree.description,
                    "dist": int(tree.dist)
                }
            )
            for tree in query
        )
    ]


async def find_command(command: str, limit=5) -> List[Tuple[str, dict]]:
    query = modules_db.CommandsTree.select(
        modules_db.CommandsTree, fn.levenshtein_dist(modules_db.CommandsTree.command, command).alias("dist")
    ).join(modules_db.Module).order_by(
        fn.levenshtein_dist(modules_db.CommandsTree.command, command)
    ).where(
        fn.levenshtein_dist(modules_db.CommandsTree.command, command) < 2
    )

    if limit != 0:
        query = query.limit(limit)

    return await _generate(query)
