import json
import os
import logging
import time
from datetime import datetime

from module_inspector import models, utils
from catalog.repos_manager import load_modules

import magic
foo = magic.Magic(mime=True)

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger("CatalogUpdater")


CATALOG_DB_DIR = "databases/catalog"
CATALOG_DB_FILE = f"modules{datetime.today().strftime('%Yx%mx%d')}.sqlite"

REPOS = {
    "geektg": ("https://github.com/GeekTG/FTG-Modules/",
               "https://github.com/GeekTG/FTG-Modules/blob/main/",
               "https://raw.githubusercontent.com/GeekTG/FTG-Modules/main/"),

    "offrepo": ("https://gitlab.com/friendly-telegram/modules-repo/",
                "https://gitlab.com/friendly-telegram/modules-repo/-/blob/master/",
                "https://gitlab.com/friendly-telegram/modules-repo/-/raw/master/"),

    "d4n13l3k00": ("https://git.d4n13l3k00.ru/D4n13l3k00/FTG-Modules/",
                   "https://git.d4n13l3k00.ru/D4n13l3k00/FTG-Modules/src/branch/master/",
                   "https://git.d4n13l3k00.ru/D4n13l3k00/FTG-Modules/raw/branch/master/"),

    "fl1yd": ("https://github.com/Fl1yd/FTG-Modules/",
              "https://github.com/Fl1yd/FTG-Modules/blob/master/",
              "https://raw.githubusercontent.com/Fl1yd/FTG-Modules/master/"),

    "keyzend": ("https://github.com/KeyZenD/modules/",
                "https://github.com/KeyZenD/modules/blob/master/",
                "https://raw.githubusercontent.com/KeyZenD/modules/master/"),

    "tio_temma": ("https://github.com/diademma/ftg-/",
                  "https://github.com/diademma/ftg-/blob/main/",
                  "https://raw.githubusercontent.com/diademma/ftg-/main/"),

    "smoke": ("https://github.com/sm1ke000FriendlyTelegram/Friendly-Telegram/",
              "https://github.com/sm1ke000FriendlyTelegram/Friendly-Telegram/blob/main/",
              "https://raw.githubusercontent.com/sm1ke000FriendlyTelegram/Friendly-Telegram/main/"),

    "atikd": ("https://github.com/AtikD/ftgmodules/",
              "https://github.com/AtikD/ftgmodules/blob/main/",
              "https://raw.githubusercontent.com/AtikD/ftgmodules/main/"),

    "hitalo": ("https://github.com/HitaloSama/FTG-modules-repo/",
               "https://github.com/HitaloSama/FTG-modules-repo/blob/master/",
               "https://raw.githubusercontent.com/HitaloSama/FTG-modules-repo/master/")
}


def modules(force=True):
    if (not force) and os.path.isfile(
        os.path.join(CATALOG_DB_DIR, CATALOG_DB_FILE)
    ):
        return logger.info("Skipped indexing")
    from catalog import modules_db

    load_modules()

    logger.info("Starting indexing modules")
    count = 0
    unidentified_list = []
    start = time.time()

    for mod_dir in ["tio_temma"]:
        mod_path = os.path.join("modules", mod_dir)
        if not os.path.isdir(mod_path):
            continue

        for mod in os.listdir(mod_path):
            file_path = os.path.join(mod_path, mod)

            if os.path.isdir(file_path):
                continue

            if mod.endswith(".py") or foo.from_file(file_path) == "text/x-python":
                logger.debug(file_path)
                _, mod_url, raw_url = [_ + mod for _ in REPOS[mod_dir]]

                lines = utils.get_lines(os.path.join("modules", mod_dir, mod))

                module = models.Module.parse(lines, mod_url, raw_url)

                if not module:
                    logging.debug("Не распознано!")

                    unidentified_list.append(file_path)
                    continue

                with modules_db.db:
                    db_module = modules_db.Module.create(
                        name=module.name,
                        description=module.description,
                        have_watcher=module.have_watcher,
                        url=module.url,
                        raw_url=module.raw_url
                    )
                    for command, description in module.commands_tree.items():
                        modules_db.CommandsTree.create(
                            module=db_module,
                            command=command,
                            description=description
                        )

                logger.debug(json.dumps(module.__dict__, indent=4, ensure_ascii=False))

                count += 1

    end = time.time()
    logger.info(f"Time elapsed {end-start}s")
    logger.info(f"Total scanned: {count}")
    logger.info(f"Total unidentified: {len(unidentified_list)}")
    logger.debug(f"Unidentified files: {', '.join(unidentified_list)}")
