import logging

import git
import shutil
import os
import config


logger = logging.getLogger(__name__)


def load_modules():
    for repo_name, urls in config.REPOS.items():
        logger.info(f"Cloning {repo_name}")
        load_module(repo_name, urls[0])


def load_module(repo_name, url):
    repo_dir = os.path.join(config.MODULES_DIR, repo_name)
    if os.path.isdir(repo_dir):
        try:
            repo = git.Repo(repo_dir)
            o = repo.remotes.origin
            o.pull()

        except git.exc.GitCommandError:
            shutil.rmtree(repo_dir, ignore_errors=True)
            clone_repo(repo_name, url)

    else:
        clone_repo(repo_name, url)


def clone_repo(repo_name, url):
    try:
        git.Git(config.MODULES_DIR).clone(url, repo_name)

    except git.exc.GitCommandError:
        logger.warning("Failed to clone repo")

