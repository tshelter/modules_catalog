import json
import os
import logging
import time

import config
from module_inspector import models, utils
from catalog.repos_manager import load_modules

import magic
foo = magic.Magic(mime=True)

logger = logging.getLogger("CatalogUpdater")


def modules(force=False):
    if (not force) and os.path.isfile(
        os.path.join(config.CATALOG_DB_DIR, config.CATALOG_DB_FILE)
    ):
        return logger.info("Skipped indexing")
    from catalog import modules_db

    load_modules()

    logger.info("Starting indexing modules")
    count = 0
    unidentified_list = []
    start = time.time()

    for mod_dir in os.listdir("modules"):
        mod_path = os.path.join("modules", mod_dir)
        if not os.path.isdir(mod_path):
            continue

        for mod in os.listdir(mod_path):
            file_path = os.path.join(mod_path, mod)

            if os.path.isdir(file_path):
                continue

            if mod.endswith(".py") or foo.from_file(file_path) == "text/x-python":
                logger.debug(file_path)
                _, mod_url, raw_url = [_ + mod for _ in config.REPOS[mod_dir]]

                lines = utils.get_lines(os.path.join("modules", mod_dir, mod))

                module = models.Module.parse(lines, mod_url, raw_url)

                if not module:
                    logging.debug("Не распознано!")
                    unidentified_list.append(file_path)
                    continue

                with modules_db.db:
                    db_module = modules_db.Module.create(
                        name=module.name,
                        description=module.description,
                        have_watcher=module.have_watcher,
                        url=module.url,
                        raw_url=module.raw_url
                    )
                    for command, description in module.commands_tree.items():
                        modules_db.CommandsTree.create(
                            module=db_module,
                            command=command,
                            description=description
                        )

                logger.debug(json.dumps(module.__dict__, indent=4, ensure_ascii=False))

                count += 1

    end = time.time()
    logger.info(f"Time elapsed {end-start}s")
    logger.info(f"Total scanned: {count}")
    logger.info(f"Total unidentified: {len(unidentified_list)}")
    logger.debug(f"Unidentified files: {', '.join(unidentified_list)}")

